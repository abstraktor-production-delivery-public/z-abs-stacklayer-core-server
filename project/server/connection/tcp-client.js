
'use strict';

const Net = require('net');


class TcpClient {
  constructor(cbConnected, cbError) {
    this.socket = null;
    this.cbConnected = cbConnected;
    this.cbError = cbError;
    this.srcAddress = '';
    this.srcPort = -1;
    this.dstAddress = '';
    this.dstPort = -1;
    this.state = TcpClient.STATE_NOT_CONNECTED;
    this.timerId = -1;
    this.downTicks = 0;
    this.chunkQueue = [];
    this.connectQueue = [];
  }
  
  start(srcAddress, srcPort, dstAddress, dstPort) {
    this.srcAddress = srcAddress;
    this.srcPort = srcPort;
    this.dstAddress = dstAddress;
    this.dstPort = dstPort;
    this._connectionHandler();
  }
  
  stop(done) {
    if(-1 !== this.timerId) {
      clearTimeout(this.timerId);
      this.timerId = -1;
    }
    if(this.socket) {
      this.socket.end(() => {
        this.state = TcpClient.STATE_NOT_CONNECTED;
        this.socket = null;
        done();
      });
    }
    else {
      this.state = TcpClient.STATE_NOT_CONNECTED;
      process.nextTick(() => {
        done();
      });
    }
  }
  
  send(msg, cbChunk, cbSent, cbConnection) {
    if(TcpClient.STATE_CONNECTED === this.state) {
      this.chunkQueue.push(cbChunk);
      if(!this.socket.write(msg)) {
        this.socket.once('drain', cbSent);
      }
      else {
        process.nextTick(cbSent);
      }
    }
    else if(TcpClient.STATE_NOT_CONNECTED === this.state) {
      if(-1 !== this.timerId) {
        clearTimeout(this.timerId);
        this.timerId = -1;
      }
      this.connectQueue.push({
        msg, cbChunk, cbSent, cbConnection
      });
      this._connectionHandler();
    }
    else if(TcpClient.STATE_CONNECTING === this.state) {
      this.connectQueue.push({
        msg, cbChunk, cbSent, cbConnection
      });
    }
  }
  
  _connectionHandler() {
    if(TcpClient.STATE_NOT_CONNECTED === this.state) {
      this.state = TcpClient.STATE_CONNECTING;
      this._connect((connected) => {
        if(!connected) {
          this.state = TcpClient.STATE_NOT_CONNECTED;
          this.downTicks += 1;
          this.timerId = setTimeout(() => {
            this.timerId = -1;
            this._connectionHandler();
          }, 100);
          while(1 <= this.connectQueue.length) {
            const sendObject = this.connectQueue.shift();
            sendObject.cbConnection(this.state);
          }
        }
        else {
          this.state = TcpClient.STATE_CONNECTED;
          this.downTicks = 0;
          while(1 <= this.connectQueue.length) {
            const sendObject = this.connectQueue.shift();
            this.send(sendObject.msg, sendObject.cbChunk, sendObject.cbSent, sendObject.cbConnection);
          }
        }
        if(this.cbConnected) {
          this.cbConnected(connected, this.downTicks);
        }
      });
    }
  }
  
  _connect(cbConnected) {
    this.chunkQueue = [];
    this.socket = Net.connect({
      host: this.dstAddress,
      port: this.dstPort,
      localAddress: this.srcAddress,
      localPort: this.srcPort,
      allowHalfOpen: true
    }, () => {
      cbConnected(true);
    });
    this.socket.setNoDelay();
    this.socket.on('data', (buffer) => {
      if(0 === this.chunkQueue.length) {
        console.log('chunk dropped.');
      }
      else {
        if(this.chunkQueue[0](buffer)) {
          this.chunkQueue.shift();
        }
      }
    });
    this.socket.on('error', (err) => {
      this.cbError(err);
    });
    this.socket.on('end', () => {
      this.socket.end();
    });
    this.socket.on('close', () => {
      cbConnected(false);
    });
  }
  
  close(done) {
    if(null !== this.socket) {
      this.socket.once('close', (hadError) => {
        process.nextTick(() => {
          done();
        });
      });
      this.socket.end();
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
}

TcpClient.STATE_CONNECTING = 1;
TcpClient.STATE_CONNECTED = 2;
TcpClient.STATE_NOT_CONNECTED = 3;
TcpClient.STATE_NAMES = ['CONNECTING', 'CONNECTED', 'NOT CONNECTED'];

module.exports = TcpClient;
